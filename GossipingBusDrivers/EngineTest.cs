﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;

namespace GossipingBusDrivers
{
    [TestFixture]
    public class EngineTest
    {
        private Mock<IRoute> _route1, _route2, _route3;
        private IDriver _driver1, _driver2, _driver3;
        private IEnumerable<IDriver> _driversList;
        private Engine _engine;
        private IEnumerable<int> _stopsList1, _stopsList2, _stopsList3;
        private int _gossip1, _gossip2, _gossip3;

        [SetUp]
        public void SetUp()
        {
            _gossip1 = 1;
            _gossip2 = 2;
            _gossip3 = 3;

            _stopsList1 = new List<int> { 3, 1, 2, 3 };
            _stopsList2 = new List<int> { 3, 2, 3, 1 };
            _stopsList3 = new List<int> { 4, 2, 3, 4, 5 };

            _route1 = new Mock<IRoute>();
            _route2 = new Mock<IRoute>();
            _route3 = new Mock<IRoute>();

            _route1.SetupGet(p => p.StopsList).Returns(_stopsList1);
            _route2.SetupGet(p => p.StopsList).Returns(_stopsList2);
            _route3.SetupGet(p => p.StopsList).Returns(_stopsList3);

            _driver1 = new Driver(_route1.Object, _gossip1);
            _driver2 = new Driver(_route2.Object, _gossip2);
            _driver3 = new Driver(_route3.Object, _gossip3);

            _driversList = new List<IDriver>() { _driver1, _driver2, _driver3 };
            _engine = new Engine(_driversList);
        }

        [TearDown]
        public void TearDown()
        {
            _gossip1 = _gossip2 = _gossip3 = 0;
            _stopsList1 = _stopsList2 = _stopsList3 = null;
            _route1 = _route2 = _route3 = null;
            _driver1 = _driver2 = _driver3 = null;
            _driversList = null;
            _engine = null;
        }

        [Test]
        public void should_return_the_stops_required_to_propagate_all_gossips()
        {
            string output = _engine.Play();

            Assert.AreEqual(5.ToString(), output);
        }

        [Test]
        public void should_return_never_if_paths_are_not_achievable()
        {
            _stopsList1 = new List<int> { 2, 1, 2 };
            _stopsList2 = new List<int> { 5, 2, 8 };

            _route1.SetupGet(p => p.StopsList).Returns(_stopsList1);
            _route2.SetupGet(p => p.StopsList).Returns(_stopsList2);

            _driver1 = new Driver(_route1.Object, _gossip1);
            _driver2 = new Driver(_route2.Object, _gossip2);

            _driversList = new List<IDriver>() { _driver1, _driver2 };
            _engine = new Engine(_driversList);

            string output = _engine.Play();

            Assert.AreEqual("Never", output);

        }
        [Test]
        public void should_return_nine_for_a_given_set_of_routes()
        {
            _stopsList1 = new List<int> { 1, 2, 3, 4, 5, 6 };
            _stopsList2 = new List<int> { 2, 3, 4, 5, 7, 6 };
            _stopsList3 = new List<int> { 7, 8, 9, 1, 3, 4, 5, 9, 3 };

            _route1.SetupGet(p => p.StopsList).Returns(_stopsList1);
            _route2.SetupGet(p => p.StopsList).Returns(_stopsList2);
            _route3.SetupGet(p => p.StopsList).Returns(_stopsList3);

            _driver1 = new Driver(_route1.Object, _gossip1);
            _driver2 = new Driver(_route2.Object, _gossip2);
            _driver3 = new Driver(_route3.Object, _gossip3);

            _driversList = new List<IDriver>() { _driver1, _driver2, _driver3 };
            _engine = new Engine(_driversList);

            string output = _engine.Play();

            Assert.AreEqual("12", output);

        }
    }
}
