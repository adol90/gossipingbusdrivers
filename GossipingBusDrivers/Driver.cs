﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;

namespace GossipingBusDrivers
{
    public class Driver : IDriver
    {
        public List<int> Gossips { get; set; }
        public IRoute Route { get; }
        public int CurrentStop { get; private set; }

        private readonly int numberOfStops;
        private int currentStopIndex;
        
        public Driver(IRoute route, int gossip)
        {           
            Route = route;           
            CurrentStop = route.StopsList.First();
            Gossips = new List<int>{gossip};
            numberOfStops = route.StopsList.Count();
            currentStopIndex = 0;
        }

        public void GoForward()
        {
            currentStopIndex = (currentStopIndex + 1) % numberOfStops;
            CurrentStop = Route.StopsList.ElementAt(currentStopIndex);
        }
    }
}