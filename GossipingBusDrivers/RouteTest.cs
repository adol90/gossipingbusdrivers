﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using NUnit.Framework;

namespace GossipingBusDrivers
{
    [TestFixture]
    public class RouteTest
    {
        private List<int> _stopsList;
        private Route _route;
        [SetUp]
        public void SetUp()
        {
            _stopsList = new List<int>()
            {
                1,2,3,4,5
            };
            _route = new Route(_stopsList);
        }

        [TearDown]
        public void TearDown()
        {
            _stopsList = new List<int>();
            _route = null;
        }

        [Test]
        public void should_throw_an_exception_if_route_is_created_with_an_empty_list_of_stops()
        {
            _stopsList = new List<int>();

            Assert.Throws<ArgumentException>(() => { _route = new Route(_stopsList); });
        }


        [Test]
        public void should_return_a_list_of_stops_after_call_get_stops()
        {
            IEnumerable<int> retrievedStops  = _route.StopsList;

            Assert.IsTrue(retrievedStops.Any());
        }


    }

}