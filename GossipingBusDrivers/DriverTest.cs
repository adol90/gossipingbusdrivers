﻿using System;
using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;

namespace GossipingBusDrivers
{
    [TestFixture]
    public class DriverTest
    {
        private Mock<IRoute> _route;
        private Driver _driver;
        private IEnumerable<int> _stopsList;
        private int _gossip;

        [SetUp]
        public void SetUp()
        {
            _gossip = 1;
            _stopsList = new List<int> {1,2,3,4,5};
            _route = new Mock<IRoute>();
            _route.SetupGet(p => p.StopsList).Returns(_stopsList);
            _driver = new Driver(_route.Object,_gossip);
        }

        [TearDown]
        public void TearDown()
        {
            _stopsList = new List<int>();
            _route = null;
        }

        [Test]
        public void should_return_its_gossips_if_checked()
        {
            Assert.IsTrue(_driver.Gossips.Count > 0);
        }

        [Test]
        public void should_be_placed_in_the_same_stop_after_complete_all_stops_cycle()
        {
            int previousStop = _driver.CurrentStop;

            int totalStops = _stopsList.Count();

            for(int i=0 ; i<totalStops ; i++)
            {
                _driver.GoForward();
            }

            Assert.AreEqual(previousStop , _driver.CurrentStop);
        }

    }
}