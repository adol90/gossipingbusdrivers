﻿using System.Collections.Generic;

namespace GossipingBusDrivers
{
    public interface IRoute
    {
        IEnumerable<int> StopsList { get; }
    }
}