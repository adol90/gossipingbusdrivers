﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using NUnit.Framework;

namespace GossipingBusDrivers
{
    internal class Engine
    {
        private readonly int expectedGossips;
        private IEnumerable<IDriver> driversList;
        public int TotalStops { get; private set; }
        public int TotalGossips { get; private set; }

        public Engine(IEnumerable<IDriver> driversList)
        {
            TotalGossips = 0;
            TotalStops = 0;
            this.driversList = driversList;
            expectedGossips = driversList.Count() * driversList.Count();
        }

        public string Play()
        {
            while (expectedGossips > TotalGossips && TotalStops < 480)
            {
                NextTurn();
            }

            return TotalStops < 480
                ? TotalStops.ToString()
                : "Never";
        }

        private void NextTurn()
        {
            
            TotalStops++;
            MergeGossipLists();
            DriversGoForward();
            RecalculateGossips();
        }

        private void MergeGossipLists()
        {
            List<IDriver> updatedListOfDrivers = new List<IDriver>();
            List<int> stopsSpy = new List<int>();

            foreach (var driver in driversList)
            {
                if (stopsSpy.Contains(driver.CurrentStop))
                {
                    if (stopsSpy.Count(x => x == driver.CurrentStop) == 1)
                    {
                        MergeTwoDriversGossips(driver, stopsSpy, updatedListOfDrivers);
                    }
                    else
                    {
                        MergeAllDriversGossips(updatedListOfDrivers, driver, stopsSpy);
                    }
                }

                stopsSpy.Add(driver.CurrentStop);
                updatedListOfDrivers.Add(driver);
            }

            driversList = updatedListOfDrivers;
        }
        private void MergeTwoDriversGossips(IDriver driver, List<int> stopsSpy, List<IDriver> updatedListOfDrivers)
        {
            driver.Gossips = driversList.ElementAt(stopsSpy.FindIndex(x => x == driver.CurrentStop)).Gossips
                .Union(driver.Gossips).ToList();
            updatedListOfDrivers[updatedListOfDrivers.FindIndex(x => x.CurrentStop == driver.CurrentStop)]
                .Gossips = driver.Gossips;
        }

        private void MergeAllDriversGossips(List<IDriver> updatedListOfDrivers, IDriver driver, List<int> stopsSpy)
        {
            foreach (var updatedDriver in updatedListOfDrivers)
            {
                driver.Gossips = driversList.ElementAt(stopsSpy.FindIndex(x => x == driver.CurrentStop)).Gossips
                    .Union(driver.Gossips).ToList();
                updatedDriver.Gossips = driver.Gossips;
            }
        }

        private void DriversGoForward()
        {
            foreach(var driver in driversList)
            { 
                driver.GoForward();
            }
        }

        private void RecalculateGossips()
        {
            TotalGossips = 0;

            foreach (var driver in driversList)
            {
                TotalGossips += driver.Gossips.Count;
            }
        }
    }
}