﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GossipingBusDrivers
{
    public class Route : IRoute
    {
        public IEnumerable<int> StopsList { get; }

        public Route(List<int> stopsList)
        {
            if (stopsList.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.");

            StopsList = stopsList;
        }
    }
}