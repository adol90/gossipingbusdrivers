﻿using System;
using System.Collections.Generic;

namespace GossipingBusDrivers
{
    public interface IDriver
    {
        List<int> Gossips { get; set; }
        IRoute Route { get; }
        int CurrentStop { get; }
        void GoForward();
    }
}